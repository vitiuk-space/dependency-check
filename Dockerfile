FROM openjdk:8-jre-alpine

ENV VERSION_DEPENDENCY 5.2.3

RUN apk add --update nodejs nodejs-npm zip bash

ADD http://dl.bintray.com/jeremy-long/owasp/dependency-check-$VERSION_DEPENDENCY-release.zip /tmp/dependency-check-cli-${VERSION_DEPENDENCY}.zip

RUN unzip /tmp/dependency-check-cli-${VERSION_DEPENDENCY}.zip -d /src && \
    bash /src/dependency-check/bin/dependency-check.sh -s="/home/" -d="/src/data" --project="test" --format=ALL --out="/tmp" && \
    rm -rf /tmp/*

WORKDIR /src
